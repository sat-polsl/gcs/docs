+++
title = "Deployment on Docker"
date = '{{ dateFormat "2006" now }}'
weight = 10
+++

## Using Docker Compose

```bash
docker compose up --detached
```

### Server Suite

```yaml
version: '3'

services:
  postgres:
    image: docker.io/library/postgres:15
    restart: always
    networks:
      - gcs
    ports:
      - '5432:5432'
    volumes:
      - ./data/postgres:/var/lib/postgresql/data:rw
    environment:
      POSTGRES_USER: user
      POSTGRES_PASSWORD: password
      POSTGRES_DB: database

  gcs_proxy:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-proxy:latest
    restart: always
    depends_on:
      - postgres
    networks:
      - gcs
    ports:
      - '14789:14789'
      - '14790:14790'
    environment:
      - PUB_PORT=14789
      - SUB_PORT=14790

  gcs_transciever:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-transceiver/runner:latest
    restart: always
    depends_on:
      - gcs_proxy
    init: true
    networks:
      - gcs
    environment:
      - PROXY_IP=gcs_proxy
      - PUB_PORT=14789
      - SUB_PORT=14790

  gcs_api_server:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-api-server:latest
    restart: always
    depends_on:
      - gcs_proxy
      - postgres
    networks:
      - gcs
    ports:
      - '8080:8080'
    environment:
      - PROXY_IP=gcs_proxy
      - PUB_PORT=14789
      - SUB_PORT=14790
      - AGENT_ADDRESS=agent:8080

  gcs_listener:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-listener:latest
    restart: always
    depends_on:
      - gcs_proxy
      - postgres
    networks:
      - gcs
    environment:
      - PROXY_IP=gcs_proxy
      - PUB_PORT=14789
      - SUB_PORT=14790
      - DB_ADDRESS=postgres:5432
      - DB_USER=user
      - DB_PASSWORD=password
      - DB_DATABASE=database

  gcs_controller:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-controller:latest
    restart: always
    depends_on:
      - gcs_proxy
    init: true
    networks:
      - gcs
    environment:
      - PROXY_IP=gcs_proxy
      - PUB_PORT=14789
      - SUB_PORT=14790

volumes:

networks:
  gcs:
```

### Agent and Measurment Suite

```yaml
version: '3'

services:
  agent:
    image: registry.gitlab.com/sat-polsl/gcs/gcs-agent-docker:latest
    restart: always
    environment: {}
    networks:
      - gcs
    ports:
      - '30200:30200'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

volumes:

networks:
  gcs:
    external: true
```
