+++
title = "Relays"
date = '{{ dateFormat "2006" now }}'
weight = 60
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-relays" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-relays" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-relays/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

GCS Relays is a tool for managing the state of power supplies for SatNOGS Rotator, 2m band filter and 70cm band filter power supply.
