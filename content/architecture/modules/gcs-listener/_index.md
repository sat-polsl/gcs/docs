+++
title = "Listener"
date = '{{ dateFormat "2006" now }}'
weight = 70
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-listener" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-listener" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-listener/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

Listener is a tool, that has only one task - save incoming frames into the DataBase after properly transforming them to fit inside traditional SQL database.
