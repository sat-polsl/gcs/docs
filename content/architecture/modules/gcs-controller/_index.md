+++
title = "Controller"
date = '{{ dateFormat "2006" now }}'
weight = 80
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-controller" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-controller" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-controller/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

GCS Controller is responsible for transforming GPS coordinates received from observed object and calculating the position of the SatNOGS rotator to follow the tracked object.
