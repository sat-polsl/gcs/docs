+++
title = "API Server"
date = '{{ dateFormat "2006" now }}'
weight = 10
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-core" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-core" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-core/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

Full API documentation can be found here: http://sat-polsl.gitlab.io/gcs/gcs-core/. The API conforms to the OpenAPI standards.
