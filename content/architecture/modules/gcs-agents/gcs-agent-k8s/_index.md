+++
title = "Kubernetes Agent"
date = '{{ dateFormat "2006" now }}'
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-kubernetes" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-kubernetes" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-kubernetes/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

{{% notice warning %}}
This feature is not yet supported.
{{% /notice %}}
