+++
title = "Agents"
date = '{{ dateFormat "2006" now }}'
weight = 20
+++

Agents are tools responsible for deploying and managing the SatNOGS clients and GCS Relays. Those modules should never be deployed "by hand", but using the Agent. Each agent designed to work with specific orchestrator/management tool.
