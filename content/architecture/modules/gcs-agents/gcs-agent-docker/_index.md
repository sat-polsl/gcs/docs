+++
title = "Docker Agent"
date = '{{ dateFormat "2006" now }}'
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-docker" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-docker" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-agent-docker/-/forks/new" data-icon="octicon-repo-forked">Fork</a>
