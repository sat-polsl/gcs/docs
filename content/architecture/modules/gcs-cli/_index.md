+++
title = "CLI"
date = '{{ dateFormat "2006" now }}'
weight = 50
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-cli" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-cli" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-cli/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

GCS CLI is a command line application used to interact with GCS API Server. It allows user to create new missions, retrieve data from past missions and control the state of the whole Ground Control System.
