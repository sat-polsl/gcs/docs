+++
title = "Proxy"
date = '{{ dateFormat "2006" now }}'
weight = 30
+++

<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-proxy" aria-label="Follow">Follow</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-proxy" data-icon="octicon-star">Star</a>
<a class="github-button" href="https://gitlab.com/sat-polsl/gcs/gcs-proxy/-/forks/new" data-icon="octicon-repo-forked">Fork</a>

Proxy is a central part of Ground Control System. It is an implementation of proxy from [ZeroMQ Pub-Sub Network with Proxy](https://zguide.zeromq.org/docs/chapter2/):

![Pub-Sub with Proxy](https://zguide.zeromq.org/images/fig13.png)
