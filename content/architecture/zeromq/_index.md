+++
title = "ZeroMQ"
date = '{{ dateFormat "2006" now }}'
weight = 20
+++

[ZeroMQ](https://zeromq.org)

## Frames

Our data frames are defined using [JSON-schema format](https://json-schema.org). Latest JSON schemas can be found in the [Common Lib repository](https://gitlab.com/sat-polsl/gcs/gcs-lib-common).

### `rx_frame`

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.com/sat-polsl/gcs/gcs-lib-common/-/raw/main/json/relays_state.schema.json",
    "title": "relays_state",
    "description": "Frame with relays state",
    "type": "object",
    "properties": {
        "rotator": {
            "description": "Enabled rotator power supply.",
            "type": "boolean"
        },
        "filter2m": {
            "description": "Enabled 2m band filter power supply.",
            "type": "boolean"
        },
        "filter70cm": {
            "description": "Enabled 70 cm band filter power supply",
            "type": "boolean"
        }
    },
    "required": [
        "rotator",
        "filter2m",
        "filter70cm"
    ]
}
```

### `rotor_ctl`

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.com/sat-polsl/gcs/gcs-lib-common/-/raw/main/json/rotor_ctl.schema.json",
    "title": "rotor_ctl",
    "description": "Decoded RX frame received by radio.",
    "type": "object",
    "properties": {
        "downrange": {
            "description": "Downrage calculated for rotor.",
            "type": "number"
        },
        "distance": {
            "description": "Distance from spacecraft.",
            "type": "number"
        },
        "azimuth": {
            "description": "Azimuth calculated for rotor.",
            "type": "number"
        },
        "elevation": {
            "description": "Elevation calculated for rotor.",
            "type": "number"
        }
    },
    "required": [
        "downrange",
        "distance",
        "azimuth",
        "elevation"
    ]
}
```

### `set_relays`

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.com/sat-polsl/gcs/gcs-lib-common/-/raw/main/json/set_relays.schema.json",
    "title": "set_relays",
    "description": "Frame to enable or disable relays.",
    "type": "object",
    "properties": {
        "rotator": {
            "description": "Enable/disable rotator power supply.",
            "type": "boolean"
        },
        "filter2m": {
            "description": "Enable/disable 2m band filter power supply.",
            "type": "boolean"
        },
        "filter70cm": {
            "description": "Enable/disable 70cm band filter power supply.",
            "type": "boolean"
        }
    },
    "required": []
}
```

### `relays_state`

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.com/sat-polsl/gcs/gcs-lib-common/-/raw/main/json/relays_state.schema.json",
    "title": "relays_state",
    "description": "Frame with relays state",
    "type": "object",
    "properties": {
        "rotator": {
            "description": "Enabled rotator power supply.",
            "type": "boolean"
        },
        "filter2m": {
            "description": "Enabled 2m band filter power supply.",
            "type": "boolean"
        },
        "filter70cm": {
            "description": "Enabled 70 cm band filter power supply",
            "type": "boolean"
        }
    },
    "required": [
        "rotator",
        "filter2m",
        "filter70cm"
    ]
}
```
