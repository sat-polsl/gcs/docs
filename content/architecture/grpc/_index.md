+++
title = "gRPC"
date = '{{ dateFormat "2006" now }}'
weight = 10
+++

## Server/Agent communication

Communication between Server and Agent is done throuh simple [gRPC](https://grpc.io) calls, defined in the file located in [gcs-lib-common](https://gitlab.com/sat-polsl/gcs/gcs-lib-common) repository.

```proto
// Note: START and END tags are used in comments to define sections.
// They are not part of the syntax for Protocol Buffers.

// [START declaration]
syntax = "proto3";
package component_api;
// [END declaration]

// [START go_declaration]
option go_package = "gitlab.com/sat-polsl/gcs/gcs-core/generated/protobufs";
// [END go_declaration]

// [START messages]
message Component {
    string name = 1;
    uint32 status = 2;
}

message Components {
    repeated Component component = 1;
}

// we cannot have RPC without parameters, so we need to create Empty message
message Empty {}
// [END messages]

// [START services]
service Manager {
    rpc Start(Component) returns (Component);
    rpc Stop(Component) returns (Component);
    rpc List(Empty) returns (Components);
}
// [END services]
```

### Go

Generation of the gRPC code requires two additional protocol buffers compiler plugins for Go. You can install them using following commands:

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
```
