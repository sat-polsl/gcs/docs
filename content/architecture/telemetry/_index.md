+++
title = "Telemetry"
date = '{{ dateFormat "2006" now }}'
weight = 40
+++

First few links to tools:
- [Jaeger](https://www.jaegertracing.io)
- [Prometheus](https://prometheus.io)
- [Grafana](https://grafana.com)
- [OpenTelemetry](http://opentelemetry.io)
- [OpenMetrics](http://openmetrics.io)

Learn more about the tools from podcasts:
- [[EN] Kubernetes Podcast (#37) - Prometheus and OpenMetrics, with Richard Hartmann](https://kubernetespodcast.com/episode/037-prometheus-and-openmetrics/)
- [[EN] Kubernetes Podcast (#97) - Jaeger, with Yuri Shkuro](https://kubernetespodcast.com/episode/097-jaeger/)
- [[PL] Patoarchitekci (#20) - Observability - Logi](https://patoarchitekci.io/20/)
- [[PL] Patoarchitekci (#21) - Observability - Metryki](https://patoarchitekci.io/21/)
- [[PL] Patoarchitekci (#22) - Observability - Distributed tracing](https://patoarchitekci.io/22/)
- [[EN] On-Call Me Maybe - OpenTelemetry & Nomad with Luiz Aoqui of HashiCorp](https://open.spotify.com/episode/7ww8y3fy49MgEbcyFGvsNV?si=70d06ea91fd84612)
