+++
title = "Architecture"
date = '{{ dateFormat "2006" now }}'
weight = 10
+++

## ZeroMQ communication

ZeroMQ is used to propagate frames received from Software Defined Radio. Those frames are send to the Proxy, and every other service is listening for them.

```mermaid
sequenceDiagram
    Transceiver->>Proxy: Publish frame
    Proxy->>+Listener: Receive frame
    Listener->>Listener: Process frame
    Listener->>-Database: Save processed frame
    Proxy->>+Rotor Controller: Receive frame
    Listener->>Listener: Process frame
    Rotor Controller->>-Proxy: Send processed frame
```

## REST API

REST API is used to control behavior of the system, creating new missions, as well as retrieving data from the past missions.

```mermaid
sequenceDiagram
    CLI->>+API Server: Send request
    alt Mission endpoint
        alt POST
            API Server->>Database: create mission
        else GET
            API Server->>Database: get mission data
            Database->>API Server: return data
        end
    else Agent endpoint
        alt POST
            API Server->>Agent: set state
            Agent->>API Server: return state
        else GET
            API Server->>Agent: get state
            Agent->>API Server: return state
        end
    else Relays endpoint
        alt POST
            API Server->>Relays: set state
            Relays->>API Server: return state
        else GET
            API Server->>Relays: get state
            Relays->>API Server: return state
        end
    end
    API Server->>-CLI: Receive response
```

### gRPC API

gRPC API is a way to have communication that is consistent, easy to version and defined by code. Internal communication between API Server and other services should be achieved using gRPC calls.

```mermaid
sequenceDiagram
    gRPC Client->>+gRPC Server: Send request
    gRPC Server->>gRPC Server: Validate request
    alt Request is valid
        gRPC Server->>gRPC Server: Apply changes
    end
    gRPC Server->>-gRPC Client: Receive response
```
