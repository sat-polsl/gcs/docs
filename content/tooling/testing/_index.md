+++
title = "Testing"
date = '{{ dateFormat "2006" now }}'
+++

## TDD

TDD stands for *Test Driven Development*. To learn more about TDD, take a look at the following resources:

- [[Wikipedia] Test-driven development](https://en.wikipedia.org/wiki/Test-driven_development)
- [[YouTube] Test-Driven Development // Fun TDD Introduction with JavaScript](https://www.youtube.com/watch?v=Jv2uxzhPFl4)

## BDD

BDD stands for *Behavior Driven Development*. To learn more about BDD, take a look at the following resources:

- [[Wikipedia] Behavior-driven development](https://en.wikipedia.org/wiki/Behavior-driven_development)
- [[YouTube] Test Driven Development vs Behavior Driven Development](https://www.youtube.com/watch?v=Bq_oz7nCNUA)

## Testify

[Testify](https://github.com/stretchr/testify) is set of libraries that extend *testing* package from standard library.

- **Assert** - The assert package provides some helpful methods that allow you to write better test code in Go.
    - Prints friendly, easy to read failure descriptions
    - Allows for very readable code
    - Optionally annotate each assertion with a message
- **Require** - The require package provides same global functions as the assert package, but instead of returning a boolean result they terminate current test.
- **Suite** - The suite package provides functionality that you might be used to from more common object oriented languages. With it, you can build a testing suite as a struct, build setup/teardown methods and testing methods on your struct, and run them with 'go test' as per normal.
- **Mock** - The mock package provides a mechanism for easily writing mock objects that can be used in place of real objects when writing test code.

## Mailtrap

[Mailtrap](http://mailtrap.io) - Email Sandbox. What should I add more?

## DockerTest

[Dockertest](https://github.com/ory/dockertest) - Use Docker to run your Golang integration tests against third party services on Microsoft Windows, Mac OSX and Linux.

## Cucumber/Gherkin

[Gherkin Reference](https://cucumber.io/docs/gherkin/reference/)

## GherkinGen

[GherkinGen](https://github.com/hedhyw/gherkingen) - a Behavior Driven Development (BDD) tests generator for Golang.
