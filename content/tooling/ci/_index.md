+++
title = "CI"
date = '{{ dateFormat "2006" now }}'
+++

We are running our CI pipelines using GitLab CI system. In this section we are describing how the CI is standardized across our GCS projects, and we provide reference configs for all of our existing and future repositories.

## Common Tasks

### Commit Lint

Most of our repos are enforcing [ConventionalCommits](https://www.conventionalcommits.org/en/v1.0.0/) - a specification for adding human and machine readable meaning to commit messages. This standardizes how we write commit messages, and helps us understand what was done in each commit. Additionally, it reduces time spent on thinking how to formulate commit message, and how should we enable 

```yaml
commitlint:
  stage: verify
  image: docker.io/library/node:18-alpine
  before_script:
    - apk add --no-cache git
    - npm install --save-dev @commitlint/config-conventional @commitlint/cli
  script:
    - npx commitlint --from ${CI_MERGE_REQUEST_TARGET_BRANCH_SHA} --to HEAD --verbose
  only:
    - main
    - merge_requests
```

### Building container

We build container, only when the tag is created.

```yaml
oci-build:
  stage: build
  image: registry.gitlab.com/sat-polsl/ci-cd/oci-builder:latest
  script:
    - ./scripts/oci-build.sh
  rules:
    - if: ${CI_COMMIT_TAG}
  variables:
    REGISTRY: "registry.gitlab.com"
    REPOSITORY: "sat-polsl/ci-cd"
    IMAGE_NAME: "oci-builder"
    IMAGE_TAG: ${CI_COMMIT_TAG}
```


{{% notice warning %}}
The runner must allow running privileged containers!
{{% /notice %}}

The OCI build script is common for all of our repositories. This way, we can adjust the parameters in the CI config, without the need to adjust them in the build scripts.

You can take a look at the OCI build script, which is located in *scripts/oci-build.sh* in every repo:

```bash
#!/bin/bash -aex

# Validate if required variables are set
if [[ -z "${REGISTRY}" ]]; then
  echo "REGISTRY not set"
  exit 1
fi
if [[ -z "${REPOSITORY}" ]]; then
  echo "REPOSITORY not set"
  exit 1
fi
if [[ -z "${IMAGE_NAME}" ]]; then
  echo "IMAGE_NAME not set"
  exit 1
fi
if [[ -z "${IMAGE_TAG}" ]]; then
  echo "IMAGE_TAG not set"
  exit 1
fi

# Set manifest name
export MANIFEST_NAME="${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:${IMAGE_TAG}"

# Login into the container registry:
buildah login \
    --username ${ROBOT_USERNAME} \
    --password ${ROBOT_PASSWORD}

# Create a multi-architecture manifest
buildah manifest create ${MANIFEST_NAME}

# Build amd64 architecture container
buildah bud \
    --tag "${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:latest" \
    --manifest ${MANIFEST_NAME} \
    --arch amd64 \
    ${BUILD_PATH}

# Build arm64 architecture container
buildah bud \
    --tag "${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:${IMAGE_TAG}" \
    --tag "${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:latest" \
    --manifest ${MANIFEST_NAME} \
    --arch arm64 \
    ${BUILD_PATH}

# Push the full manifest, with both CPU Architectures, with IMAGE_TAG tag
buildah manifest push \
    --all \
    ${MANIFEST_NAME} \
    "docker://${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:${IMAGE_TAG}"

# Push the full manifest, with both CPU Architectures, with latest tag
buildah manifest push \
    --all \
    ${MANIFEST_NAME} \
    "docker://${REGISTRY}/${REPOSITORY}/${IMAGE_NAME}:latest"
```
