+++
title = "Pre-Commit"
date = '{{ dateFormat "2006" now }}'
+++

## Pre-commit hooks

Pre Commit hooks are occuring, when after we call `git commit` command. They help us enforce standards, like commit message format or code formatting. To learn more about pre-commit app, visit [the pre-commit tool documentation website](https://pre-commit.com).

The installation is done using the following file, located in *scripts/install-hooks.sh* in every repo:

```bash
#!/bin/bash -aex

pre-commit install --hook-type pre-commit
pre-commit install --hook-type commit-msg
```

### Common Hooks

Our common hooks include validating of JSON, TOML and YAML files, as well validating if the commit message conforms to Conventional Commits standards.

```yaml
repos:
  - repo: https://github.com/compilerla/conventional-pre-commit
    rev: v2.1.1
    hooks:
      - id: conventional-pre-commit
        stages: [commit-msg]
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.3.0
    hooks:
      - id: check-json
        stages: [commit]
      - id: check-toml
        stages: [commit]
      - id: check-yaml
        stages: [commit]
```

### Go

In Go, we simply run [GolangCI Lint](http://golangci-lint.run) to make as many checks as possible.

```yaml
  - repo: https://github.com/golangci/golangci-lint
    rev: v1.50.0
    hooks:
      - id: golangci-lint
        stages: [commit]
```

### C++

{{% notice warning %}}
C++ Pre Commit Hooks has not been configured yet.
{{% /notice %}}
