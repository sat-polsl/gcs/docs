+++
title = "commitlint"
date = '{{ dateFormat "2006" now }}'
+++

[commitlint](https://commitlint.js.org/#/) is configured through the *.commitlintrc.yml* file located in the root of every repository:

```yaml
extends:
  - "@commitlint/config-conventional"
```
